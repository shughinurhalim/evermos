def is_path(row, col, map, prev):
    if row == -1 or col == -1 or row == 6 or col == 8 or prev[row][col] or map[row][col] == '#':
        return False
    return True

def find_path(row, col, row_goal, col_goal, map, path, all_path, prev):
    if row == row_goal and col == col_goal:
        all_path.append(path)
        return

    prev[row][col] = True

    if is_path(row-1, col, map, prev):
        path += 'U'
        find_path(row-1, col, row_goal, col_goal, map, path, all_path, prev)
        path = path[:-1]

    if is_path(row, col + 1, map, prev):
        path += 'R'
        find_path(row, col + 1, row_goal, col_goal, map, path, all_path, prev)
        path = path[:-1]

    if is_path(row + 1, col, map, prev):
        path += 'D'
        find_path(row + 1, col, row_goal, col_goal, map, path, all_path, prev)
        path = path[:-1]

    if is_path(row, col - 1, map, prev):
        path += 'L'
        find_path(row, col - 1, row_goal, col_goal, map, path, all_path, prev)
        path = path[:-1]

    prev[row][col] = False

def convert(string):
    if string == 'D':
        return 'Down'
    elif string == 'U':
        return 'Up'
    elif string == 'L':
        return 'Left'
    elif string == 'R':
        return 'Right'

def showPath(map, row_goal, col_goal) -> None:
    all_path = []
    path = ''
    prev = [[False for _ in range(8)]
                   for _ in range(6)]

    find_path(4, 1, row_goal, col_goal, map, path, all_path, prev)

    prev = ''
    cnt = 0
    for i in range(len(all_path)):
        for j in range(len(all_path[i])):
            if j == 0:
                prev = all_path[i][j]
                cnt = 1
                print(convert(prev), end=' ')
            else:
                if prev == all_path[i][j]:
                    cnt += 1
                else:
                    print(cnt, end=' time(s), ')
                    prev = all_path[i][j]
                    cnt = 1
                    print(convert(prev), end=' ')
        print(cnt, end=" time(s)\n")

map = [
    ['#','#','#','#','#','#','#','#'],
    ['#','.','.','.','.','.','.','#'],
    ['#','.','#','#','#','.','.','#'],
    ['#','.','.','.','#','.','#','#'],
    ['#','X','#','.','.','.','.','#'],
    ['#','#','#','#','#','#','#','#'],
]

clear_path = []

for i in range(len(map)):
    for j in range(len(map[i])):
        print(map[i][j], end='')

        if map[i][j] == '.':
            clear_path.append([i, j])
    print('')

for k in range(len(clear_path)):
    print(clear_path[k])
    showPath(map, clear_path[k][0], clear_path[k][1])