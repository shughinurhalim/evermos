from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy import func
from sqlalchemy.sql.elements import and_
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import coalesce

from models.Product import Product

import time

from flask_jwt_extended import *

engine = create_engine('mysql+mysqlconnector://root:000000@localhost:3306/evermos', echo=True)
Session = sessionmaker(bind = engine)
session = Session()

class Transaction:
    def checkout(param):
        model = Product()

        return model.checkout(param)