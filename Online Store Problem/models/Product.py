from mysql.connector import connect
import time

class Product:
    def __init__(self):
        try:
            self.db = connect(
                host = 'localhost',
                database = 'evermos',
                user = 'root',
                password = '000000'
            )
        except Exception as e:
            print(e)

    def checkout(self, param):
        # get product quantity
        query = '''
            SELECT qty FROM product WHERE code = '{}' FOR UPDATE
        '''.format(param['code'])

        print(query)

        cursor = self.db.cursor()
        cursor.execute(query)
        data = cursor.fetchone()

        if data == None:
            return {
                'message' : 'Product not found'
            }

        qty = data[0]

        # sleep so can hit the API twice (make it run together at the same time)
        # time.sleep(10)

        # if product quantity > order quantity
        if qty >= param['qty']:
            query = '''
                UPDATE product SET qty = {} WHERE code = '{}'
            '''.format(qty - param['qty'], param['code'])

            cursor = self.db.cursor()
            cursor.execute(query)

            query = '''
                INSERT INTO orders (code, qty, status) VALUES ('{}', {}, '{}')
            '''.format(param['code'], param['qty'], 'PROCESSING')

            cursor = self.db.cursor()
            cursor.execute(query)

            self.db.commit()

            return {
                'message' : "Checkout Success"
            }
        else:
            return{
                'message' : "Checkout failed due to unsufficient product quantity"
            }