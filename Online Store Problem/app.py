from flask import Flask, jsonify, request
from controllers.TransactionController import Transaction

from jsonschema import validate, ValidationError, FormatChecker

app = Flask(__name__)

schema = {
    'checkout' : {
        'type' : 'object',
        'properties' : {
            'code' : {
                'type' : 'string',
                'minLength' : 1,
                'maxLength' : 45
            },
            'qty' : {
                'type' : 'integer'
            }
        },
        'required' : ['code', 'qty']
    }
}

@app.route('/checkout', methods=["POST"])
def checkout():
    param = request.json

    try:
        validate(param, schema['checkout'], format_checker=FormatChecker())
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transaction.checkout(param))    

if __name__ == '__main__':
    app.run(debug=True)