# Evermos

Evermos Backend Engineer Position Technical test

For Evermos online store problem where product quantity doesnt match with order quantity when having an event like 12.12, i think that happens because there are many process that run together at the same time and when not locking the data for product quantity when querying might cause this incident to happen.
if we dont lock the data when there are many process runs together and not commited yet, other process might selecting the previous quantity of the product which the quantity of the product might be updated but not committed yet.
i would recommend to use `SELECT FOR UPDATE` query to lock the data that will cause other process to hold their proccess until the process that locking the table successfully committed. that way all the proccess that runs concurrently will wait until the process is done and select the updated quantity of the product.

example: <br />
`SELECT qty FROM product where id = 1` <br />
when this query runs together at the same time, it will produce the same output even though the first process might update the quantity <br />

but <br />
`SELECT qty from product where id = 1 FOR UPDATE` <br />
when we use this query together at the same time, the next process will wait until the process that runs first finish doing update to the table and will show the after update data <br />

**Note**
- For online store problem runs with Flask framework and the URL can be access with `/checkout`
- Online store checkout api receive 2 parameter with json format:
  1. `code (string, length = [1,45])`
  2. `qty (integer)`
- Table that i created for testing are as follows:

`product` <br />
```
+-------------+---------------+------+-----+---------+----------------+ 
| Field       | Type          | Null | Key | Default | Extra          | 
+-------------+---------------+------+-----+---------+----------------+ 
| id          | int           | NO   | PRI | NULL    | auto_increment | 
| name        | varchar(100)  | NO   |     | NULL    |                | 
| code        | varchar(45)   | NO   |     | NULL    |                | 
| qty         | int           | NO   |     | NULL    |                | 
| owner_id    | int           | NO   |     | NULL    |                | 
| description | varchar(1000) | NO   |     | NULL    |                | 
+-------------+---------------+------+-----+---------+----------------+ 
```

`orders` <br />
```
+--------+-------------+------+-----+---------+----------------+ 
| Field  | Type        | Null | Key | Default | Extra          | 
+--------+-------------+------+-----+---------+----------------+ 
| id     | int         | NO   | PRI | NULL    | auto_increment | 
| code   | varchar(45) | NO   |     | NULL    |                | 
| qty    | int         | NO   |     | NULL    |                | 
| status | varchar(45) | NO   |     | NULL    |                | 
+--------+-------------+------+-----+---------+----------------+
```